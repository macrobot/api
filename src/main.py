import discord
import aiohttp
from aiohttp import web
import json
import logging
import ssl

# Path to the config file
CONFIG_PATH = "/app/config.json"

with open(CONFIG_PATH, "r") as f:
    # Read and parse config file
    CONFIG = json.load(f)


if(CONFIG.get("debug")):
    # Set logging level
    logging.basicConfig(level=logging.INFO)
else:
    logging.basicConfig(level=logging.WARN)
    

class Bot(object):
    # Main bot class
    def __init__(self, api_key: str):
        """Initializes and starts the discord bot and the web API
        Args:
            api_key (string): The discord API key

        Returns:
            None
        """
        # Initialization
        self.api_key = api_key
        self.client = discord.Client()
        self.api_server = ApiServer(self)

        # Set up events
        self.client.event(self.on_ready)
        self.client_task = self.client.loop.create_task(self.client.start(self.api_key))

        # Start services
        self.api_server.run()

    async def on_ready(self):
        """This method is called once the bot is up and running, and ready for connections

        Args:
            None

        Returns:
            None
        """
        print(f"Bot started with user {self.client.user}")


class ApiServer:
    """Class for the API server, it forwards requests to the appropriate bot endpoints"""
    def __init__(self, bot):
        self.bot = bot
        self.api = web.Application()
        self.web_config = CONFIG.get("apiConfig")
        self.base_url = self.web_config.get("apiBase")

        self.ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        self.ssl_context.load_cert_chain('/app/cert.pem', '/app/server.key')

        self.api.add_routes([
            web.get(f"{self.base_url}/version", self.version),
            web.get(f"{self.base_url}/login", self.login),
        ])

        self.authorized_users = {}

    def run(self):
        web.run_app(self.api,
                    port=self.web_config.get("port"),
                    ssl_context=self.ssl_context)

    async def version(self, request):
        return web.json_response({
            "project": "macrobot/api",
            "version": "0.0.1",
            "authors": [
                "vorap <me@thvxl.se>",
                "pelp3 <>"
            ]
        })

    async def exchange_code(self, code):
        data = aiohttp.FormData()

        data.add_field('client_id', CONFIG.get("clientId"))
        data.add_field('client_secret', CONFIG.get("clientSecret"))
        data.add_field('grant_type', 'authorization_code')
        data.add_field('code', code)
        data.add_field('redirect_uri', CONFIG.get("redirectUri"))
        data.add_field('scope', 'identify guilds')

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        async with aiohttp.ClientSession(headers=headers) as session:
            async with session.post("{}/oauth2/token".format(CONFIG.get("apiEndpoint")),
                                    data=data) as resp:
                response = await resp.json()
        return response

    async def get_user(self, access_token):
        headers = {
            "Authorization": f"Bearer {access_token}"
        }
        async with aiohttp.ClientSession(headers=headers) as session:
            async with session.get("{}/users/@me".format(CONFIG.get("apiEndpoint"))) as resp:
                response = await resp.json()
        return response

    async def login(self, request):
        authorization_code = request.rel_url.query["code"]
        response = await self.exchange_code(authorization_code)
        user = await self.get_user(response.get("access_token"))
        return web.json_response(user)


if __name__ == "__main__":
    bot = Bot(CONFIG.get("apiKey"))