FROM python:buster
RUN mkdir app

RUN apt update -y
RUN apt install -y ffmpeg

COPY src/. /app
RUN pip3 install -r /app/requirements.txt

ENTRYPOINT [ "python3", "/app/main.py" ]